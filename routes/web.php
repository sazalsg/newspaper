<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('website.index');
});


Route::get('/authentic','user_panel@index');
Route::resource('/newspaper_sub_category','newspaper_sub_category');
Route::resource('/addvertisment','addvertisment');
Route::resource('/Newspaper-setup', 'Newspaper_setup');
Route::resource('/category', 'category');
