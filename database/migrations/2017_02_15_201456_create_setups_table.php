<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name',200);
            $table->string('company_abbr',10);
            $table->string('established',100);
            $table->string('email',100);
            $table->string('mobile',20);
            $table->string('telephone',30);
            $table->string('reg_number',30);
            $table->text('facebook_api');
            $table->text('twitter');
            $table->text('office_location_google_api');
            $table->string('logo_name',30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setups');
    }
}
