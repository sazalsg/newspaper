<!DOCTYPE html>
<html lang=”en”>
<html lang="en-US">

<head>

				<!-- / Yoast SEO plugin. Start  -->
				<meta name="google-site-verification" content="" />
				<meta charset="UTF-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1" />
				<link rel="pingback" href="xmlrpc.php" />
				<meta name="description" content="SEO Content"/>
				<meta name="robots" content="noodp"/>
				<!-- / Yoast SEO plugin. End  -->


				<title>Customize Titel</title>
				<link rel="shortcut icon" href="{{URL::asset('website/image/Braincoder.png')}}" />




				<!-- css Link For This Site Start  -->
				<script src="https://use.fontawesome.com/fd9dba5260.js"></script>
				<link rel='dns-prefetch' href='http://fonts.googleapis.com/' /> <!--Font Link -->
				<link rel='stylesheet' id='awesome-weather-css'  href="{{URL::asset('website/wp-content/plugins/awesome-weather/awesome-weather1c9b.css?ver=4.6.1')}}" type='text/css' media='all' />
				<link rel='stylesheet' id='contact-form-7-css'  href="{{URL::asset('website/wp-content/plugins/contact-form-7/includes/css/styles11b8.css?ver=4.5')}}" type='text/css' media='all' />
				<link rel='stylesheet' id='prayer-time-css'  href="{{URL::asset('website/wp-content/plugins/muslim-prayer-time-bd/css/prayer-timed5f7.css?ver=2.0')}}" type='text/css' media='all' />
				<link rel='stylesheet' id='super-rss-reader-css-css'  href="{{URL::asset('website/wp-content/plugins/super-rss-reader/public/srr-css1c9b.css?ver=4.6.1')}}" type='text/css' media='all' />
				<link rel='stylesheet' id='wp-polls-css'  href="{{URL::asset('website/wp-content/plugins/wp-polls/polls-csse0ef.css?ver=2.73.1')}}" type='text/css' media='all' />
				<link rel='stylesheet' id='smartmag-fonts-css'  href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Roboto+Slab' type='text/css' media='all' />
				<link rel='stylesheet' id='smartmag-core-css'  href="{{URL::asset('website/wp-content/themes/FeniNews-child/style1c9b.css?ver=4.6.1')}}" type='text/css' media='all' />
				<link rel='stylesheet' id='smartmag-responsive-css'  href="{{URL::asset('website/wp-content/themes/FeniNews/css/responsive1c9b.css?ver=4.6.1')}}" type='text/css' media='all' />
				<link rel='stylesheet' id='pretty-photo-css'  href="{{URL::asset('website/wp-content/themes/FeniNews/css/prettyPhoto1c9b.css?ver=4.6.1')}}" type='text/css' media='all' />
				<link rel='stylesheet' id='font-awesome-css'  href="{{URL::asset('website/wp-content/themes/FeniNews/css/fontawesome/css/font-awesome.min1c9b.css?ver=4.6.1')}}" type='text/css' media='all' />
				<link rel='stylesheet' id='pwLogWi_style-css'  href="{{URL::asset('website/wp-content/plugins/nice-login-register-widget/css/pw-login-widget264f.css?ver=1.3.10')}}" type='text/css' media='all' />
				<link rel='stylesheet' id='wpmu-wpmu-ui-3-min-css-css'  href="{{URL::asset('website/wp-content/plugins/custom-sidebars/inc/external/wpmu-lib/css/wpmu-ui.3.min1c9b.css?ver=4.6.1')}}" type='text/css' media='all' />
				<link rel='stylesheet' id='wpmu-animate-3-min-css-css'  href="{{URL::asset('website/wp-content/plugins/custom-sidebars/inc/external/wpmu-lib/css/animate.3.min1c9b.css?ver=4.6.1')}}" type='text/css' media='all' />
				<link rel='stylesheet' id='default-styles-css'  href="{{URL::asset('website/wp-content/plugins/business-directory-plugin/themes/default/assets/styles1c9b.css?ver=4.6.1')}}" type='text/css' media='all' />
				<!-- css Link For This Site End  -->


				<!-- Js Link For This Site start -->
				 <script type='text/javascript' src="{{URL::asset('website/wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4')}}"></script>
				 <script type='text/javascript' src="{{URL::asset('website/wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1')}}"></script>
				 <script type='text/javascript' src="{{URL::asset('website/wp-content/plugins/super-rss-reader/public/srr-js1c9b.js?ver=4.6.1')}}"></script>
				 <script type='text/javascript' src="{{URL::asset('website/wp-content/plugins/wp-retina-2x/js/picturefill.min5b75.js?ver=3.0.2')}}"></script>
				 <script type='text/javascript' src="{{URL::asset('website/wp-content/themes/FeniNews/js/jquery.prettyPhoto1c9b.js?ver=4.6.1')}}"></script>
				<!-- Js Link For This Site End -->


</head>


<body class="home page page-id-27 page-template page-template-page-blocks page-template-page-blocks-php page-builder right-sidebar boxed">
<style>

@font-face {
	font-family: 'BebasNeueRegular';
	src: url('http://static.tumblr.com/umdphle/Wttm2vv3h/bebasneue-webfont.eot');
	src: url('http://static.tumblr.com/umdphle/Wttm2vv3h/bebasneue-webfont.eot') format('embedded-opentype'),
			 url('http://static.tumblr.com/umdphle/YX4m2vv68/bebasneue-webfont.woff') format('woff'),
			 url('http://static.tumblr.com/umdphle/8kOm2vv6p/bebasneue-webfont.ttf') format('truetype'),
			 url('http://static.tumblr.com/umdphle/dtSm2vv5u/bebasneue-webfont.svg') format('svg');
	font-weight: normal;
	font-style: normal;
}

</style>

<div class="main-wrap">
	<!-- Top bar Start -->
				<div class="top-bar" style="border-top: 4px solid #19232D; background: #19232D;">
					<div class="wrap">
						<section class="top-bar-content"> <!-- section Start -->

									<div class="trending-ticker">
											<span class="heading" style="font-family:BebasNeueRegular">Breaking News</span>

												<ul>
														<li><a style="color: #fff" href="#" title="The political tumult that rocked the world in 2016 might be an appetizer">The political tumult that rocked the world in 2016 might be an appetizer</a></li>
													<li><a style="color: #fff" href="#" title="The political tumult that rocked the world in 2016 might be an appetizer">The political tumult that rocked the world in 2016 might be an appetizer</a></li>
														<li><a style="color: #fff" href="#" title="The political tumult that rocked the world in 2016 might be an appetizer">The political tumult that rocked the world in 2016 might be an appetizer</a></li>
														<li><a style="color: #fff" href="#" title="The political tumult that rocked the world in 2016 might be an appetizer">The political tumult that rocked the world in 2016 might be an appetizer</a></li>
														<li><a style="color: #fff" href="#" title="The political tumult that rocked the world in 2016 might be an appetizer">The political tumult that rocked the world in 2016 might be an appetizer</a></li>

								  </div>

									<div class="search">
										<form action="http://news.fenionline.net/" method="get">
											<input type="text" name="s" class="query" value="" placeholder="Search..." />
											<button class="search-button" type="submit"><i class="fa fa-search"></i></button>
										</form>
									</div>
						</section> <!-- section End -->
					</div>
				</div>
	<!-- Top bar End  -->


	<div id="main-head" class="main-head">
			<div class="wrap"> <!-- Wrap Start-->
									<header><!-- Header Start -->
												<!-- Logo Part Start -->
													<div class="title">
															<a href="#" title="Times Of Bangladesh" rel="home">
																	<img src="{{URL::asset('website/Image/times_of_bangladesh.jpg')}}" style="height: 105px;width: 340px;" class="logo-image" alt="Braincoder"  />
															</a>
															<div style="font-size:14px; font-weight:normal; line-height:normal; margin:-10px 0px 10px 5px; color:#aaaaaa;">
																<i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp; &nbsp;

																<!-- Date Started -->
																<script type="text/javascript">

																function date_time(id)
																{
																        date = new Date;
																        year = date.getFullYear();
																        month = date.getMonth();
																        months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'Jully', 'August', 'September', 'October', 'November', 'December');
																        d = date.getDate();
																        day = date.getDay();
																        days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
																        h = date.getHours();
																        if(h<10)
																        {
																                h = "0"+h;
																        }
																        m = date.getMinutes();
																        if(m<10)
																        {
																                m = "0"+m;
																        }
																        s = date.getSeconds();
																        if(s<10)
																        {
																                s = "0"+s;
																        }
																        result = ''+days[day]+' '+months[month]+' '+d+' '+year+' '+h+':'+m+':'+s;
																        document.getElementById(id).innerHTML = result;
																        setTimeout('date_time("'+id+'");','1000');
																        return true;
																}
																																</script>
																<!-- Date Ended -->

																<span id="date_time"></span>
																<script type="text/javascript">window.onload = date_time('date_time');</script>
									 						</div>
													</div>
											<!-- Logo Part Ending -->


											<!-- Use Add  Start-->
												<div class="right">
														<div class="ads-widget">
															<a href="#"><img src="{{URL::asset('website/Image/300.jpg')}}" /></a>
														</div>
												</div>
											<!-- Use Add End -->
									</header><!-- Header End -->





								<!-- Menubar Nav Start  -->
								<nav class="navigation cf" data-sticky-nav="1">
									<div class="mobile"><a href="#" class="selected"><span class="text">Navigate</span><span class="current"></span> <i class="fa fa-bars"></i></a></div>

									<div class="menu-main-menu-container">
											<ul id="menu-main-menu" class="menu">
														<li><a href="index.html"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
														<li><a href="#"><i class="fa fa-globe" aria-hidden="true"></i>&nbsp;Bangladesh</a> <!--Dropdown Menu Start-->
																<ul class="sub-menu">
																		<li  ><a href="#">Feni</a></li>
																		<li ><a href="#">DhaKa</a></li>
																		<li  ><a href="#">Chittagong</a></li>
																		<li  ><a href="#">Khulna</a></li>
																		<li><a href="#">Barisal</a></li>
																		<li ><a href="#">Rajshai</a></li>
																</ul>
													 </li><!--Dropdown Menu Start-->
													  <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Politics</a></li>
														<li ><a href="#"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> &nbsp;Crime</a></li>
														<li ><a href="#"><i class="fa fa-futbol-o" aria-hidden="true"></i>&nbsp;Sports</a></li>
														<li><a href="#"><i class="fa fa-flask" aria-hidden="true"></i>&nbsp;Science & Technology</a></li>
														<li ><a href="#"><i class="fa fa-american-sign-language-interpreting" aria-hidden="true"></i>&nbsp;Entertainment</a></li>
														<li ><a href="#"><i class="fa fa-cc" aria-hidden="true"></i>&nbsp;cultural</a></li>
														<li><a href="#"><i class="fa fa-compass" aria-hidden="true"></i> &nbsp;Directory</a></li>
														<li><a href="#"><i class="fa fa-square-o" aria-hidden="true"></i>&nbsp;ফেনী ব্লগ</a></li>
											</ul>
										</div>
							</nav>
						<!-- Menubar nav End  -->
			</div><!-- Wrap Start-->
		</div>



<div class="main wrap cf">
		<div class="row">
			<div class="col-8 main-content">
				<div id="post-27" class="page-content post-27 page type-page status-publish">


         <!--- নির্বাচিত  News  Start -->
					<div class="posts-list listing-alt">
						<article>
							<span class="cat-title cat-1"><a href="#">Selected</a></span>
							<a href="#" itemprop="url"><img style="height: 190px;width: 325px;"  src="{{URL::asset('image/1.jpg')}}" class="attachment-main-block size-main-block wp-post-image" alt="" title="" /></a>
							<div class="content">
								<time>August 14, 2016 </time>
									<!-- <span class="comments"><a href="#"><i class="fa fa-comments-o"></i>0</a></span> -->
									<a href="#" title="#">The political tumult that rocked the world in 2016 might be an appetize</a>
									<div class="excerpt">
										<p>Crucial elections loom this year in France and Germany, where the same anti-establishment backlash that produced Donald Trump and Brexit could offer an opening to&hellip;</p>
											<div class="read-more"><a href="#" title="Read More">Read More</a></div>
									</div>
							</div>
						</article>
					</div>
			<!--- নির্বাচিত  News  End -->



				<section class="news-focus"> <!-- Secation Start -->
						<div class="section-head heading cat-21">
								<a href="category/feni-city/index.html" title="">Bangladesh</a>
						</div>

						<div class="row news-0 highlights">
							<div class="column half blocks">
									<article itemscope itemtype="#"> <!-- article Start-->
										<span class="cat-title larger cat-16">
											<a href="category/fulgazi/index.html">Dhaka</a></span>
											<a href="#" title="#" class="image-link" itemprop="url">
												<img width="351" height="185" src="image/2.jpg" class="image wp-post-image" alt="farhad nagar" title="" srcset="image/2.jpg" sizes="(max-width: 351px) 100vw, 351px" />
											</a>
											<div class="meta">
													<time>August 10 2016</time>
													<span class="comments">
														<a href="#"><i class="fa fa-eye" style=""> 400</i></a>&nbsp;&nbsp;&nbsp;
														<!-- <a href="#"><i class="fa fa-comments-o"></i>0</a></span> -->

											</div>
											<h2 itemprop="name"><a href="#" title="">The political tumult that rocked the world in 2016 might be an appetizer</a></h2>
											<div class="excerpt">
												<p>Crucial elections loom this year in France and Germany, where the same anti-establishment backlash that produced Donald Trump and Brexit could offer an opening to&hellip;</p>
													<div  class="read-more"><a style="padding: 1px 9px 1px 10px;background: #E54E53;color: #fff;" href="#" title="Read More">Read More</a></div>
											</div>

									</article><!-- article End-->
							</div>


							<ul class="column half block posts-list thumb">
								<li>
									<a href="#"><img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="feni rab" title="#" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" /></a>
									<div class="content">
										<time>August 18 2016</time>
                        <span class="comments">
													<a href="#"><i class="fa fa-eye" style=""> 40</i></a>&nbsp;&nbsp;&nbsp;
													<a href="#"><i class="fa fa-comments-o"></i>	0</a></span>
													<a href="#" title="">The political tumult that rocked the world in 2016 might be an appetizer</a>
									</div>
								</li>


								<li>
									<a href="#"><img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="feni rab" title="#" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" /></a>
									<div class="content">
										<time>August 18 2016</time>
                        <span class="comments">
													<a href="#"><i class="fa fa-eye" style=""> 40</i></a>&nbsp;&nbsp;&nbsp;
													<a href="#"><i class="fa fa-comments-o"></i>	0</a></span>
													<a href="#" title="">The political tumult that rocked the world in 2016 might be an appetizer</a>
									</div>
								</li>



								<li>
									<a href="#"><img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="feni rab" title="#" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" /></a>
									<div class="content">
										<time>August 18 2016</time>
												<span class="comments">
													<a href="#"><i class="fa fa-eye" style=""> 40</i></a>&nbsp;&nbsp;&nbsp;
													<a href="#"><i class="fa fa-comments-o"></i>	0</a></span>
													<a href="#" title="">The political tumult that rocked the world in 2016 might be an appetizer</a>
									</div>
								</li>

								<li>
									<a href="#"><img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="feni rab" title="#" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" /></a>
									<div class="content">
										<time>August 18 2016</time>
												<span class="comments">
													<a href="#"><i class="fa fa-eye" style=""> 40</i></a>&nbsp;&nbsp;&nbsp;
													<a href="#"><i class="fa fa-comments-o"></i>	0</a></span>
													<a href="#" title="">The political tumult that rocked the world in 2016 might be an appetizer</a>
									</div>
								</li>

							</ul>


					</div>
		</section><!-- Secation End -->





			<div class="row cf builder">
					<div class="column builder one-1">
							<div class="textwidget">
								<a  href="#"><img style="height: 87px;" src="image/300.jpg" /></a>
							</div>
					</div>
			</div>


			<div class="row cf highlights-box">
				<div class="column half">
					<section class="highlights">
						<span class="cat-title larger cat-16">
							<a href="category/fulgazi/index.html">Politics</a></span>
								<article>
										<a href="#" title="#" class="image-link" itemprop="url">
												<img width="351" height="185" src="image/3.PNG" class="image wp-post-image" alt="" title="" />
										</a>
										<div class="meta">
										<time>February 1 2000</time>
										<span class="comments">
											<a href="#"><i class="fa fa-eye" style=""> 100</i></a>&nbsp;&nbsp;&nbsp;
											<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
									</div>
									<h2 itemprop="name"><a href="#" title="#">The political tumult that rocked the world in 2016 might be an appetizer</a></h2>
									<div class="excerpt">
										<p>Crucial elections loom this year in France and Germany, where the same anti-establishment backlash that produced Donald Trump and Brexit could offer an  &hellip;</p>
										<div  class="read-more"><a style="padding: 1px 9px 1px 10px;background: #E54E53;color: #fff;" href="#" title="Read More">Read More</a></div>
									</div>
								</article>


								<ul class="block posts-list thumb">
									<li>
										<a href="#"><img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="hang dead suicide" title="" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" /></a>
										<div class="content">
											<time>August 20 2016</time>
                        <span class="comments">
													<a href="#"><i class="fa fa-eye" style=""> 200</i></a>&nbsp;&nbsp;&nbsp;
													<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
													<a href="#" title="The political tumult that rocked the world in 2016 might be an appetizer">The political tumult that rocked the world in 2016 might be an appetizer</a>
										</div>
									</li>

									<li>
										<a href="#"><img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="hang dead suicide" title="" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" /></a>
										<div class="content">
											<time>August 20 2016</time>
                        <span class="comments">
													<a href="#"><i class="fa fa-eye" style=""> 200</i></a>&nbsp;&nbsp;&nbsp;
													<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
													<a href="#" title="The political tumult that rocked the world in 2016 might be an appetizer">The political tumult that rocked the world in 2016 might be an appetizer</a>
										</div>
									</li>


									<li>
										<a href="#"><img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="hang dead suicide" title="" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" /></a>
										<div class="content">
											<time>August 20 2016</time>
                        <span class="comments">
													<a href="#"><i class="fa fa-eye" style=""> 200</i></a>&nbsp;&nbsp;&nbsp;
													<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
													<a href="#" title="The political tumult that rocked the world in 2016 might be an appetizer">The political tumult that rocked the world in 2016 might be an appetizer</a>
										</div>
									</li>




								</ul>
				</section>
			</div>




				<div class="column half">
						<section class="highlights">

								<span class="cat-title larger cat-17"><a href="#">Crime</a></span>
												<article>
															<a href="#" title="" class="image-link" itemprop="url">
															<img width="351" height="185" src="image/4.png" class="image wp-post-image" alt="" title="" srcset="image/4.png" sizes="(max-width: 351px) 100vw, 351px" />
															</a>

															<div class="meta">
																<time>আগস্ট ১৪, ২০১৬ </time>
																<span class="comments">
																	<a href="#"><i class="fa fa-eye" style=""> 200</i></a>&nbsp;&nbsp;&nbsp;
																	<a href="#"><i class="fa fa-comments-o"></i>0</a>
																</span>
															</div>
															<h2 itemprop="name"><a href="#" title="The political tumult that rocked the world in 2016 might be an appetizer">The political tumult that rocked the world in 2016 might be an appetizer</a></h2>
															<div class="excerpt">
																<p>Crucial elections loom this year in France and Germany, where the same anti-establishment backlash that produced Donald Trump and Brexit could offer an opening to…&hellip;</p>
																<div  class="read-more"><a style="padding: 1px 9px 1px 10px;background: #E54E53;color: #fff;" href="#" title="Read More">Read More</a></div>
															</div>
												</article>


												<ul class="block posts-list thumb">
																	<li>
																					<a href="#">
																						<img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" title="News Titel" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" />
																					</a>
																					<div class="content">
																								<time>August 14 2016</time>
									                        			<span class="comments">
																									<a href="#"><i class="fa fa-eye" style=""> 100</i></a>&nbsp;&nbsp;&nbsp;
																									<a href="#"><i class="fa fa-comments-o"></i>0</a>
																								</span>
																								<a href="#" title="The political tumult that rocked the world in 2016 might be an appetizer">The political tumult that rocked the world in 2016 might be an appetizerু</a>
																					</div>
																	</li>

																	<li>
																					<a href="#">
																						<img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" title="News Titel" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" />
																					</a>
																					<div class="content">
																								<time>August 14 2016</time>
									                        			<span class="comments">
																									<a href="#"><i class="fa fa-eye" style=""> 100</i></a>&nbsp;&nbsp;&nbsp;
																									<a href="#"><i class="fa fa-comments-o"></i>0</a>
																								</span>
																								<a href="#" title="The political tumult that rocked the world in 2016 might be an appetizer">The political tumult that rocked the world in 2016 might be an appetizerু</a>
																					</div>
																	</li>


																	<li>
																					<a href="#">
																						<img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" title="News Titel" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" />
																					</a>
																					<div class="content">
																								<time>August 14 2016</time>
									                        			<span class="comments">
																									<a href="#"><i class="fa fa-eye" style=""> 100</i></a>&nbsp;&nbsp;&nbsp;
																									<a href="#"><i class="fa fa-comments-o"></i>0</a>
																								</span>
																								<a href="#" title="The political tumult that rocked the world in 2016 might be an appetizer">The political tumult that rocked the world in 2016 might be an appetizerু</a>
																					</div>
																	</li>

												</ul>
										</section>
									</div>
							</div>



							<section class="news-focus">
									<div class="section-head heading cat-18"><a href="#" title="International">International</a></div>

											<div class="row news-0 highlights">
													<div class="column half blocks">
															<article itemscope itemtype="#">
																<span class="cat-title larger cat-16">
																	<a href="#">International</a></span>
																		<a href="#" title="" class="image-link" itemprop="url">
																				<img width="351" height="185" src="image/110208e.jpg" class="image wp-post-image" alt="freedom fighter" title="The political tumult that rocked the world in 2016 might be an appetizer" />
																		</a>

																		<div class="meta">
																			<time>August 14 2016</time>
																			<span class="comments">
																				<a href="#"><i class="fa fa-eye" style=""> 140</i></a>&nbsp;&nbsp;&nbsp;
																				<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
																		</div>
																		<h2 itemprop="name"><a href="#" title="The political tumult that rocked the world in 2016 might be an appetizerা">The political tumult that rocked the world in 2016 might be an appetizerা</a></h2>
																		<div class="excerpt">
																				<p>Crucial elections loom this year in France and Germany, where the same anti-establishment backlash that produced Donald Trump and Brexit could offer an&hellip;</p>
																		</div>
															</article>
														</div>


															<ul class="column half block posts-list thumb">
																	<li>
																			<a href="#">
																				<img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" title="সোনাগাজীতে এমিটি সোলার প্লান্টের পরিকল্পনা নিয়ে ভূমি পরিদর্শন" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" />
																			</a>
																			<div class="content">
																				<time>August 04 2016 </time>
													              <span class="comments">
																					<a href="#"><i class="fa fa-eye" style=""> 200</i></a>&nbsp;&nbsp;&nbsp;
																					<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
																					<a href="#" title="The political tumult that rocked the world in 2016 might be an appetize">The political tumult that rocked the world in 2016 might be an appetizerা</a>
																			</div>
																	</li>

																	<li>
																			<a href="#">
																				<img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" title="সোনাগাজীতে এমিটি সোলার প্লান্টের পরিকল্পনা নিয়ে ভূমি পরিদর্শন" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" />
																			</a>
																			<div class="content">
																				<time>August 04 2016 </time>
													              <span class="comments">
																					<a href="#"><i class="fa fa-eye" style=""> 200</i></a>&nbsp;&nbsp;&nbsp;
																					<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
																					<a href="#" title="The political tumult that rocked the world in 2016 might be an appetize">The political tumult that rocked the world in 2016 might be an appetizerা</a>
																			</div>
																	</li>

																	<li>
																			<a href="#">
																				<img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" title="সোনাগাজীতে এমিটি সোলার প্লান্টের পরিকল্পনা নিয়ে ভূমি পরিদর্শন" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" />
																			</a>
																			<div class="content">
																				<time>August 04 2016 </time>
													              <span class="comments">
																					<a href="#"><i class="fa fa-eye" style=""> 200</i></a>&nbsp;&nbsp;&nbsp;
																					<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
																					<a href="#" title="The political tumult that rocked the world in 2016 might be an appetize">The political tumult that rocked the world in 2016 might be an appetizerা</a>
																			</div>
																	</li>
																</ul>
															</div>
													</section>






													<div class="row cf highlights-box"> <!--Highligt Box Start -->

														<div class="column half">
															<section class="highlights">
																	<span class="cat-title larger cat-19">
																			<a href="#">Sports</a></span>
																				<article>
																								<a href="#" title="The political tumult that rocked the world in 2016 might be an appetize" class="image-link" itemprop="url">
																										<img width="351" height="185" src="image/5.jpg" class="image wp-post-image" alt="" title="The political tumult that rocked the world in 2016 might be an appetize" srcset="image/5.jpg" sizes="(max-width: 351px) 100vw, 351px" />
																								</a>

																								<div class="meta">
																									<time>August 15 2016</time>
																									<span class="comments">
																										<a href="#"><i class="fa fa-eye" style=""> 200</i></a>&nbsp;&nbsp;&nbsp;
																										<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
																								</div>
																								<h2 itemprop="name"><a href="#" title="The political tumult that rocked the world in 2016 might be an appetize">The political tumult that rocked the world in 2016 might be an appetize</a></h2>
																								<div class="excerpt">
																									<p>Crucial elections loom this year in France and Germany, where the same anti-establishment backlash that produced Donald Trump and Brexit could offer an…&hellip;</p>
																									<div class="read-more"><a style="padding: 1px 9px 1px 10px;background: #E54E53;color: #fff;" href="#" title="Read More">Read More</a></div>
																								</div>
																				</article>


																				<ul class="block posts-list thumb">
																					<li>
																								<a href="#">
																									<img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="halim" title="ওমানে সড়ক দূর্ঘটনায় ছাগলনাইয়ার হালিম নিহত" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" />
																								</a>
																								<div class="content">
																									<time>August 8 2016</time>
		                        											<span class="comments">
																										<a href="#"><i class="fa fa-eye" style=""> 145</i></a>&nbsp;&nbsp;&nbsp;
																										<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
																										<a href="#" title="The political tumult that rocked the world in 2016 might be an appetize">The political tumult that rocked the world in 2016 might be an appetize</a>
																								</div>
																					</li>


																					<li>
																								<a href="#">
																									<img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="halim" title="ওমানে সড়ক দূর্ঘটনায় ছাগলনাইয়ার হালিম নিহত" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" />
																								</a>
																								<div class="content">
																									<time>August 8 2016</time>
		                        											<span class="comments">
																										<a href="#"><i class="fa fa-eye" style=""> 145</i></a>&nbsp;&nbsp;&nbsp;
																										<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
																										<a href="#" title="The political tumult that rocked the world in 2016 might be an appetize">The political tumult that rocked the world in 2016 might be an appetize</a>
																								</div>
																					</li>


																					<li>
																								<a href="#">
																									<img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="halim" title="ওমানে সড়ক দূর্ঘটনায় ছাগলনাইয়ার হালিম নিহত" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" />
																								</a>
																								<div class="content">
																									<time>August 8 2016</time>
		                        											<span class="comments">
																										<a href="#"><i class="fa fa-eye" style=""> 145</i></a>&nbsp;&nbsp;&nbsp;
																										<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
																										<a href="#" title="The political tumult that rocked the world in 2016 might be an appetize">The political tumult that rocked the world in 2016 might be an appetize</a>
																								</div>
																					</li>


																					<li>
																								<a href="#">
																									<img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="halim" title="ওমানে সড়ক দূর্ঘটনায় ছাগলনাইয়ার হালিম নিহত" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" />
																								</a>
																								<div class="content">
																									<time>August 8 2016</time>
		                        											<span class="comments">
																										<a href="#"><i class="fa fa-eye" style=""> 145</i></a>&nbsp;&nbsp;&nbsp;
																										<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
																										<a href="#" title="The political tumult that rocked the world in 2016 might be an appetize">The political tumult that rocked the world in 2016 might be an appetize</a>
																								</div>
																					</li>
																				</ul>
															</section>
													</div>


													<div class="column half">
														<section class="highlights">
																<span class="cat-title larger cat-19">
																		<a href="#">ছাগলনাইয়া</a></span>
																			<article>
																							<a href="#" title="ছাগলনাইয়ায় ডিজিটাল কম্পিউটারে আউটসোর্সিং কোর্স চালু" class="image-link" itemprop="url">
																									<img width="351" height="185" src="image/110208e.jpg" class="image wp-post-image" alt="" title="ছাগলনাইয়ায় ডিজিটাল কম্পিউটারে আউটসোর্সিং কোর্স চালু" srcset="image/110208e.jpg" sizes="(max-width: 351px) 100vw, 351px" />
																							</a>

																							<div class="meta">
																								<time datetime="২০১৬-০৮-২০T১৮:৪৮:০৮+০০:০০" itempro="datePublished">আগস্ট ২০, ২০১৬ </time>
																								<span class="comments">
																									<a href="#"><i class="fa fa-eye" style=""> ৪৯৫</i></a>&nbsp;&nbsp;&nbsp;
																									<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
																							</div>
																							<h2 itemprop="name"><a href="#" title="ছাগলনাইয়ায় ডিজিটাল কম্পিউটারে আউটসোর্সিং কোর্স চালু">ছাগলনাইয়ায় ডিজিটাল কম্পিউটারে আউটসোর্সিং কোর্স চালু</a></h2>
																							<div class="excerpt">
																								<p>ছাগলনাইয়া পৌর শহরের ইসলাম প্লাজার দ্বিতীয় তলায় ডিজিটাল কম্পিউটারে চালু করা হয়েছে আউটসোর্সিং / ফ্রিল্যান্সিং কোর্স। শুক্রবার সকাল ১০টায় প্রধান&hellip;</p>
																							</div>
																			</article>


																			<ul class="block posts-list thumb">
																				<li>
																							<a href="#">
																								<img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="halim" title="ওমানে সড়ক দূর্ঘটনায় ছাগলনাইয়ার হালিম নিহত" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" />
																							</a>
																							<div class="content">
																								<time datetime="২০১৬-০৮-০২T১৪:০২:০২+০০:০০">আগস্ট ২, ২০১৬ </time>
																								<span class="comments">
																									<a href="#"><i class="fa fa-eye" style=""> ২৯৮</i></a>&nbsp;&nbsp;&nbsp;
																									<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
																									<a href="#" title="ওমানে সড়ক দূর্ঘটনায় ছাগলনাইয়ার হালিম নিহত">ওমানে সড়ক দূর্ঘটনায় ছাগলনাইয়ার হালিম নিহত</a>
																							</div>
																				</li>


																				<li>
																							<a href="#">
																								<img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="halim" title="ওমানে সড়ক দূর্ঘটনায় ছাগলনাইয়ার হালিম নিহত" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" />
																							</a>
																							<div class="content">
																								<time datetime="২০১৬-০৮-০২T১৪:০২:০২+০০:০০">আগস্ট ২, ২০১৬ </time>
																								<span class="comments">
																									<a href="#"><i class="fa fa-eye" style=""> ২৯৮</i></a>&nbsp;&nbsp;&nbsp;
																									<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
																									<a href="#" title="ওমানে সড়ক দূর্ঘটনায় ছাগলনাইয়ার হালিম নিহত">ওমানে সড়ক দূর্ঘটনায় ছাগলনাইয়ার হালিম নিহত</a>
																							</div>
																				</li>


																				<li>
																							<a href="#">
																								<img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="halim" title="ওমানে সড়ক দূর্ঘটনায় ছাগলনাইয়ার হালিম নিহত" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" />
																							</a>
																							<div class="content">
																								<time datetime="২০১৬-০৮-০২T১৪:০২:০২+০০:০০">আগস্ট ২, ২০১৬ </time>
																								<span class="comments">
																									<a href="#"><i class="fa fa-eye" style=""> ২৯৮</i></a>&nbsp;&nbsp;&nbsp;
																									<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
																									<a href="#" title="ওমানে সড়ক দূর্ঘটনায় ছাগলনাইয়ার হালিম নিহত">ওমানে সড়ক দূর্ঘটনায় ছাগলনাইয়ার হালিম নিহত</a>
																							</div>
																				</li>


																				<li>
																							<a href="#">
																								<img width="110" height="96" src="image/110208e.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="halim" title="ওমানে সড়ক দূর্ঘটনায় ছাগলনাইয়ার হালিম নিহত" srcset="image/110208e.jpg" sizes="(max-width: 110px) 100vw, 110px" />
																							</a>
																							<div class="content">
																								<time datetime="২০১৬-০৮-০২T১৪:০২:০২+০০:০০">আগস্ট ২, ২০১৬ </time>
																								<span class="comments">
																									<a href="#"><i class="fa fa-eye" style=""> ২৯৮</i></a>&nbsp;&nbsp;&nbsp;
																									<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
																									<a href="#" title="ওমানে সড়ক দূর্ঘটনায় ছাগলনাইয়ার হালিম নিহত">ওমানে সড়ক দূর্ঘটনায় ছাগলনাইয়ার হালিম নিহত</a>
																							</div>
																				</li>
																			</ul>
														</section>
												</div>


								</div> <!-- Highlight Box End -->

								<div class="row cf builder">
											<div class="column builder one-1">
												<div class="textwidget"><a href="#"><img src="http://news.fenionline.net/wp-content/uploads/2014/07/feni_bazar_ad_block.gif" /></a></div>
											</div>
								</div>





								<div class="row cf highlights-box three-col">
									<!-- Frist Colum Start -->
									<div class="column one-third">
										<section class="highlights">

													<div class="section-head cat-text-2">
														<a href="category/politics/index.html">রাজনীতি</a>
													</div>

													<article itemscope itemtype="#">
															<a href="#" title="ফেনীতে ভিপি জয়নালের নেতৃত্বে বিএনপির বিক্ষোভ মিছিল" class="image-link" itemprop="url">
																<img width="214" height="140" src="wp-content/uploads/2016/08/vp-joynal-bnp-214x140.jpg" class="image wp-post-image" alt="vp joynal bnp" title="ফেনীতে ভিপি জয়নালের নেতৃত্বে বিএনপির বিক্ষোভ মিছিল" srcset="http://news.fenionline.net/wp-content/uploads/2016/08/vp-joynal-bnp-214x140.jpg 214w, http://news.fenionline.net/wp-content/uploads/2016/08/vp-joynal-bnp-214x140@2x.jpg 428w" sizes="(max-width: 214px) 100vw, 214px" />
															</a>
															<div class="meta">
																<time datetime="২০১৬-০৮-১৮T২২:০৯:৪৩+০০:০০" itempro="datePublished">আগস্ট ১৮, ২০১৬ </time>
																<span class="comments">
																	<a href="#"><i class="fa fa-eye" style=""> ৪৫১</i></a>&nbsp;&nbsp;&nbsp;
																	<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
															</div>
															<h2 itemprop="name"><a href="#" title="ফেনীতে ভিপি জয়নালের নেতৃত্বে বিএনপির বিক্ষোভ মিছিল">ফেনীতে ভিপি জয়নালের নেতৃত্বে বিএনপির বিক্ষোভ মিছিল</a></h2>
												 </article>


												 <ul class="block posts">
													 <li>
														 <i class="fa fa-angle-right"></i>
														 <a href="#" title="নথি না আসায় আবারো পেছালো নিজাম হাজারীর মামলার রায়" class="title">নথি না আসায় আবারো পেছালো নিজাম হাজারীর মামলার রায়</a>
													 </li>

													 <li>
														 <i class="fa fa-angle-right"></i>
														 <a href="#" title="নথি না আসায় আবারো পেছালো নিজাম হাজারীর মামলার রায়" class="title">নথি না আসায় আবারো পেছালো নিজাম হাজারীর মামলার রায়</a>
													 </li>


													 <li>
														 <i class="fa fa-angle-right"></i>
														 <a href="#" title="নথি না আসায় আবারো পেছালো নিজাম হাজারীর মামলার রায়" class="title">নথি না আসায় আবারো পেছালো নিজাম হাজারীর মামলার রায়</a>
													 </li>


													 <li>
														 <i class="fa fa-angle-right"></i>
														 <a href="#" title="নথি না আসায় আবারো পেছালো নিজাম হাজারীর মামলার রায়" class="title">নথি না আসায় আবারো পেছালো নিজাম হাজারীর মামলার রায়</a>
													 </li>


													 <li>
														 <i class="fa fa-angle-right"></i>
														 <a href="#" title="নথি না আসায় আবারো পেছালো নিজাম হাজারীর মামলার রায়" class="title">নথি না আসায় আবারো পেছালো নিজাম হাজারীর মামলার রায়</a>
													 </li>
												 </ul>
								</section>
							</div>





							<div class="column one-third">
									<section class="highlights">
											<div class="section-head cat-text-3">
													<a href="category/finance/index.html">অর্থনীতি</a>
											</div>
											<article>
														<a href="#" title="সাজেল চৌধুরীর খামারে কোরবানীর জন্য প্রস্তুত হচ্ছে কোটি টাকার দেশী গরু" class="image-link" itemprop="url">
															<img width="214" height="140" src="image/1.jpg" class="image wp-post-image" alt="sajel chowdhury" title="সাজেল চৌধুরীর খামারে কোরবানীর জন্য প্রস্তুত হচ্ছে কোটি টাকার দেশী গরু" srcset="image/1.jpg" sizes="(max-width: 214px) 100vw, 214px" />
														</a>
														<div class="meta">
															<time datetime="২০১৬-০৮-১৪T১৩:০৪:৩১+০০:০০" itempro="datePublished">আগস্ট ১৪, ২০১৬ </time>
															<span class="comments">
																<a href="#"><i class="fa fa-eye" style=""> ১,০১৩</i></a>&nbsp;&nbsp;&nbsp;
																<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
														</div>
														<h2 itemprop="name"><a href="#" title="সাজেল চৌধুরীর খামারে কোরবানীর জন্য প্রস্তুত হচ্ছে কোটি টাকার দেশী গরু">সাজেল চৌধুরীর খামারে কোরবানীর জন্য প্রস্তুত হচ্ছে কোটি টাকার দেশী গরু</a></h2>
											</article>


											<ul class="block posts">
													<li>
														<i class="fa fa-angle-right"></i>
														<a href="#" title="ফেনীতে চাহিদার বেশি মাছ উৎপাদন" class="title">ফেনীতে চাহিদার বেশি মাছ উৎপাদন</a>
													</li>

													<li>
														<i class="fa fa-angle-right"></i>
														<a href="#" title="ফেনীতে চাহিদার বেশি মাছ উৎপাদন" class="title">ফেনীতে চাহিদার বেশি মাছ উৎপাদন</a>
													</li>


													<li>
														<i class="fa fa-angle-right"></i>
														<a href="#" title="ফেনীতে চাহিদার বেশি মাছ উৎপাদন" class="title">ফেনীতে চাহিদার বেশি মাছ উৎপাদন</a>
													</li>
											</ul>

										</section>
								</div>



									<div class="column one-third">
											<section class="highlights">
														<div class="section-head cat-text-4">
																<a href="#">খেলাধুলা</a>
														</div>
														<article>
																	<a href="#" title="জেবি বিপিএল ফুটবলে ফেনীর জয়" class="image-link" itemprop="url">
																		<img width="214" height="140" src="image/1.jpg" class="image wp-post-image" alt="sajel chowdhury" title="সাজেল চৌধুরীর খামারে কোরবানীর জন্য প্রস্তুত হচ্ছে কোটি টাকার দেশী গরু" srcset="image/1.jpg" sizes="(max-width: 214px) 100vw, 214px" />
																	</a>
																	<div class="meta">
																			<time datetime="২০১৬-০৮-১৯T২১:১৭:০২+০০:০০" itempro="datePublished">আগস্ট ১৯, ২০১৬ </time>
																			<span class="comments">
																				<a href="#"><i class="fa fa-eye" style=""> ১৯২</i></a>&nbsp;&nbsp;&nbsp;
																				<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
																	</div>
																	<h2 itemprop="name"><a href="#" title="জেবি বিপিএল ফুটবলে ফেনীর জয়">জেবি বিপিএল ফুটবলে ফেনীর জয়</a></h2>
														</article>
														<ul class="block posts">
																<li>
																	<i class="fa fa-angle-right"></i>
																	<a href="#" title="ফেনীতে সপ্তাহব্যাপী হ্যান্ডবল প্রশিক্ষণ শুরু" class="title">ফেনীতে সপ্তাহব্যাপী হ্যান্ডবল প্রশিক্ষণ শুরু</a>
																</li>
																<li>
																	<i class="fa fa-angle-right"></i>
																	<a href="#" title="ফেনীতে সপ্তাহব্যাপী হ্যান্ডবল প্রশিক্ষণ শুরু" class="title">ফেনীতে সপ্তাহব্যাপী হ্যান্ডবল প্রশিক্ষণ শুরু</a>
																</li>
																<li>
																	<i class="fa fa-angle-right"></i>
																	<a href="#" title="ফেনীতে সপ্তাহব্যাপী হ্যান্ডবল প্রশিক্ষণ শুরু" class="title">ফেনীতে সপ্তাহব্যাপী হ্যান্ডবল প্রশিক্ষণ শুরু</a>
																</li>

														</ul>
											</section>
									</div>
							</div>

<hr class="separator" />



	<div class="row cf highlights-box three-col">
			<div class="column one-third">
					<section class="highlights">
							<div class="section-head cat-text-5">
								<a href="category/entertainment/index.html">বিনোদন</a>
							</div>

							<article>
										<a href="#" title="জমে উঠেছে দর্শকশূন্য প্রেক্ষাগৃহ" class="image-link" itemprop="url">
											<img width="214" height="140" src="image/1.jpg" class="image wp-post-image" alt="" title="জমে উঠেছে দর্শকশূন্য প্রেক্ষাগৃহ" srcset="image/1.jpg" sizes="(max-width: 214px) 100vw, 214px" />
										</a>
										<div class="meta">
											<time datetime="২০১৬-০৭-১৩T১০:৩৯:৫৯+০০:০০" itempro="datePublished">জুলাই ১৩, ২০১৬ </time>
											<span class="comments">
												<a href="#"><i class="fa fa-eye" style=""> ৩২৭</i></a>&nbsp;&nbsp;&nbsp;
												<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
										</div>
										<h2 itemprop="name"><a href="#" title="জমে উঠেছে দর্শকশূন্য প্রেক্ষাগৃহ">জমে উঠেছে দর্শকশূন্য প্রেক্ষাগৃহ</a></h2>
							</article>

							<ul class="block posts">
											<li>
												<i class="fa fa-angle-right"></i>
												<a href="#" title="সবুজ অরণ্যের বীজভূমিঃ ফেনী হর্টিকালচার সেন্টার" class="title">সবুজ অরণ্যের বীজভূমিঃ ফেনী হর্টিকালচার সেন্টার</a>
											</li>

											<li>
												<i class="fa fa-angle-right"></i>
												<a href="#" title="সবুজ অরণ্যের বীজভূমিঃ ফেনী হর্টিকালচার সেন্টার" class="title">সবুজ অরণ্যের বীজভূমিঃ ফেনী হর্টিকালচার সেন্টার</a>
											</li>


											<li>
												<i class="fa fa-angle-right"></i>
												<a href="#" title="সবুজ অরণ্যের বীজভূমিঃ ফেনী হর্টিকালচার সেন্টার" class="title">সবুজ অরণ্যের বীজভূমিঃ ফেনী হর্টিকালচার সেন্টার</a>
											</li>

											<li>
												<i class="fa fa-angle-right"></i>
												<a href="#" title="সবুজ অরণ্যের বীজভূমিঃ ফেনী হর্টিকালচার সেন্টার" class="title">সবুজ অরণ্যের বীজভূমিঃ ফেনী হর্টিকালচার সেন্টার</a>
											</li>
							</ul>
				</section>
			</div>


			<div class="column one-third">
				<section class="highlights">
									<div class="section-head cat-text-6">
										<a href="category/technology/index.html">তথ্যপ্রযুক্তি</a>
									</div>

									<article itemscope itemtype="http://schema.org/Article">
												<a href="#" title="নাসার কার্যাল‌য়ে ফেনীর ছে‌লে অপু" class="image-link" itemprop="url">
													<img width="214" height="140" src="image/1.jpg" class="image wp-post-image" alt="opu" title="নাসার কার্যাল‌য়ে ফেনীর ছে‌লে অপু" srcset="http://news.fenionline.net/wp-content/uploads/2016/08/opu-214x140.jpg 214w, http://news.fenionline.net/wp-content/uploads/2016/08/opu-214x140@2x.jpg 428w" sizes="(max-width: 214px) 100vw, 214px" />
												</a>

												<div class="meta">
													<time datetime="২০১৬-০৮-০১T১৩:৫৯:২৪+০০:০০" itempro="datePublished">আগস্ট ১, ২০১৬ </time>
													<span class="comments">
														<a href="#"><i class="fa fa-eye" style=""> ৩১৬</i></a>&nbsp;&nbsp;&nbsp;
														<a href="#"><i class="fa fa-comments-o"></i>0</a></span>
												</div>
												<h2 itemprop="name"><a href="#" title="নাসার কার্যাল‌য়ে ফেনীর ছে‌লে অপু">নাসার কার্যাল‌য়ে ফেনীর ছে‌লে অপু</a></h2>
									</article>

									<ul class="block posts">
											<li>
												<i class="fa fa-angle-right"></i>
												<a href="#" title="ফেনী ইউনিভার্সিটির আইসিটি কুইজ প্রতিযোগিতায় পলিটেকনিক চ্যাম্পিয়ন" class="title">েনী ইউনিভার্সিটির আইসিটি কুইজ প্রতিযোগিতায় পলিটেকনিক চ্যাম্পিয়ন					</a>
											</li>

											<li>
												<i class="fa fa-angle-right"></i>
												<a href="#" title="ফেনী ইউনিভার্সিটির আইসিটি কুইজ প্রতিযোগিতায় পলিটেকনিক চ্যাম্পিয়ন" class="title">েনী ইউনিভার্সিটির আইসিটি কুইজ প্রতিযোগিতায় পলিটেকনিক চ্যাম্পিয়ন					</a>
											</li>

											<li>
												<i class="fa fa-angle-right"></i>
												<a href="#" title="ফেনী ইউনিভার্সিটির আইসিটি কুইজ প্রতিযোগিতায় পলিটেকনিক চ্যাম্পিয়ন" class="title">েনী ইউনিভার্সিটির আইসিটি কুইজ প্রতিযোগিতায় পলিটেকনিক চ্যাম্পিয়ন					</a>
											</li>
									</ul>
						</section>
					</div>


					<div class="column one-third">
						<section class="highlights">
											<div class="section-head cat-text-7">
												<a href="#">শিল্প-সাহিত্য</a>
											</div>
											<article>
														<a href="#" title="জমে উঠেছে দর্শকশূন্য প্রেক্ষাগৃহ" class="image-link" itemprop="url">
															<img width="214" height="140" src="image/1.jpg" class="image wp-post-image" alt="" title="জমে উঠেছে দর্শকশূন্য প্রেক্ষাগৃহ" srcset="image/1.jpg" sizes="(max-width: 214px) 100vw, 214px" />
														</a>
														<div class="meta">
																<time datetime="২০১৬-০৭-১৩T১০:৩৯:৫৯+০০:০০" itempro="datePublished">জুলাই ১৩, ২০১৬ </time>
																<span class="comments">
																	<a href="#"><i class="fa fa-eye" style=""> ৩২৭</i></a>&nbsp;&nbsp;&nbsp;
																	<a href="#"><i class="fa fa-comments-o"></i>0</a>
																</span>
														</div>
														<h2 itemprop="name">
															<a href="#" title="জমে উঠেছে দর্শকশূন্য প্রেক্ষাগৃহ">জমে উঠেছে দর্শকশূন্য প্রেক্ষাগৃহ</a>
														</h2>
											</article>

											<ul class="block posts">
														<li>
															<i class="fa fa-angle-right"></i>
															<a href="#" title="চলচ্চিত্র প্রদর্শনীতে যোগ দিতে ইতালী যাচ্ছেন ফেনীর ছেলে সায়েম" class="title">চলচ্চিত্র প্রদর্শনীতে যোগ দিতে ইতালী যাচ্ছেন ফেনীর ছেলে সায়েম</a>
														</li>

														<li>
															<i class="fa fa-angle-right"></i>
															<a href="#" title="চলচ্চিত্র প্রদর্শনীতে যোগ দিতে ইতালী যাচ্ছেন ফেনীর ছেলে সায়েম" class="title">চলচ্চিত্র প্রদর্শনীতে যোগ দিতে ইতালী যাচ্ছেন ফেনীর ছেলে সায়েম</a>
														</li>

														<li>
															<i class="fa fa-angle-right"></i>
															<a href="#" title="চলচ্চিত্র প্রদর্শনীতে যোগ দিতে ইতালী যাচ্ছেন ফেনীর ছেলে সায়েম" class="title">চলচ্চিত্র প্রদর্শনীতে যোগ দিতে ইতালী যাচ্ছেন ফেনীর ছেলে সায়েম</a>
														</li>

														<li>
															<i class="fa fa-angle-right"></i>
															<a href="#" title="চলচ্চিত্র প্রদর্শনীতে যোগ দিতে ইতালী যাচ্ছেন ফেনীর ছেলে সায়েম" class="title">চলচ্চিত্র প্রদর্শনীতে যোগ দিতে ইতালী যাচ্ছেন ফেনীর ছেলে সায়েম</a>
														</li>
											</ul>
						</section>
					</div>
				</div>




				<section class="news-focus">
						<div class="section-head heading cat-8">
								<a href="#" title="লাইফস্টাইল">লাইফস্টাইল</a>
						</div>

						<div class="row news-0 highlights">
							<div class="column half blocks">
									<article>
													<a href="#" title="জেনে নিন ছোট ছোট শারীরিক অক্ষমতার সমাধান" class="image-link" itemprop="url">
														<img width="351" height="185" src="image/1.jpg" class="image wp-post-image" alt="fenionline net lifestyle" title="জেনে নিন ছোট ছোট শারীরিক অক্ষমতার সমাধান" srcset="image/1.jpg" sizes="(max-width: 351px) 100vw, 351px" />
													</a>
													<div class="meta">
															<time datetime="২০১৬-০৮-১৬T১৩:৩৭:৫১+০০:০০" itemprop="datePublished">আগস্ট ১৬, ২০১৬ </time>
															<span class="comments">
																<a href="#"><i class="fa fa-eye" style=""> ১৮৫</i></a>&nbsp;&nbsp;&nbsp;
																<a href="#"><i class="fa fa-comments-o"></i>0</a>
															</span>
													</div>
													<h2 itemprop="name"><a href="#" title="জেনে নিন ছোট ছোট শারীরিক অক্ষমতার সমাধান">জেনে নিন ছোট ছোট শারীরিক অক্ষমতার সমাধান</a></h2>
													<div class="excerpt">
														<p>প্রতিদিন কত রকম শারীরিক সমস্যায় পড়ি আমরা। কখনো সকালে ঘুমে থেকে উঠেই দেখি গলা বসে গেছে। কখনো বুকে ঠান্ডা জমেছে,&hellip;</p>
													</div>
									</article>
							</div>

							<ul class="column half block posts-list thumb">
								<li>
											<a href="#">
												<img width="110" height="96" src="image/1.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="starline iftar" title="প্রথম বছরেই ১শ ১ আইটেম নিয়ে স্টার লাইন সুইটস’র ইফতার" srcset="image/1.jpg" sizes="(max-width: 110px) 100vw, 110px" />
											</a>

											<div class="content">
												<time datetime="২০১৬-০৬-১৭T২২:১৩:৫৪+০০:০০">জুন ১৭, ২০১৬ </time>
		                    <span class="comments">
													<a href="#"><i class="fa fa-eye" style=""> ২০৯</i></a>&nbsp;&nbsp;&nbsp;
													<a href="#"><i class="fa fa-comments-o"></i>0</a>
												</span>
												<a href="#" title="প্রথম বছরেই ১শ ১ আইটেম নিয়ে স্টার লাইন সুইটস’র ইফতার">প্রথম বছরেই ১শ ১ আইটেম নিয়ে স্টার লাইন সুইটস’র ইফতার</a>
											</div>
								</li>

								<li>
											<a href="#">
												<img width="110" height="96" src="image/1.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="starline iftar" title="প্রথম বছরেই ১শ ১ আইটেম নিয়ে স্টার লাইন সুইটস’র ইফতার" srcset="image/1.jpg" sizes="(max-width: 110px) 100vw, 110px" />
											</a>

											<div class="content">
												<time datetime="২০১৬-০৬-১৭T২২:১৩:৫৪+০০:০০">জুন ১৭, ২০১৬ </time>
		                    <span class="comments">
													<a href="#"><i class="fa fa-eye" style=""> ২০৯</i></a>&nbsp;&nbsp;&nbsp;
													<a href="#"><i class="fa fa-comments-o"></i>0</a>
												</span>
												<a href="#" title="প্রথম বছরেই ১শ ১ আইটেম নিয়ে স্টার লাইন সুইটস’র ইফতার">প্রথম বছরেই ১শ ১ আইটেম নিয়ে স্টার লাইন সুইটস’র ইফতার</a>
											</div>
								</li>


								<li>
											<a href="#">
												<img width="110" height="96" src="image/1.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="starline iftar" title="প্রথম বছরেই ১শ ১ আইটেম নিয়ে স্টার লাইন সুইটস’র ইফতার" srcset="image/1.jpg" sizes="(max-width: 110px) 100vw, 110px" />
											</a>

											<div class="content">
												<time datetime="২০১৬-০৬-১৭T২২:১৩:৫৪+০০:০০">জুন ১৭, ২০১৬ </time>
		                    <span class="comments">
													<a href="#"><i class="fa fa-eye" style=""> ২০৯</i></a>&nbsp;&nbsp;&nbsp;
													<a href="#"><i class="fa fa-comments-o"></i>0</a>
												</span>
												<a href="#" title="প্রথম বছরেই ১শ ১ আইটেম নিয়ে স্টার লাইন সুইটস’র ইফতার">প্রথম বছরেই ১শ ১ আইটেম নিয়ে স্টার লাইন সুইটস’র ইফতার</a>
											</div>
								</li>

							</ul>
					</div>
				</section>


				<!-- Gallery start -->
					<section class="gallery-block">
							<h3 class="gallery-title prominent">সংগঠন সংবাদ</h3>
								<div class="flexslider carousel">
										<ul class="slides">
												<li>
															<a href="#" title="রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন" class="image-link">
																<span class="image">
																<img width="214" height="140" src="image/1.jpg" class="attachment-gallery-block size-gallery-block wp-post-image" alt="" title="" srcset="image/1.jpg" sizes="(max-width: 214px) 100vw, 214px" /></span>
															</a>
															<p class="title"><a href="#" title="রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন">রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন</a></p>
												</li>

												<li>
															<a href="#" title="রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন" class="image-link">
																<span class="image">
																<img width="214" height="140" src="image/1.jpg" class="attachment-gallery-block size-gallery-block wp-post-image" alt="" title="" srcset="image/1.jpg" sizes="(max-width: 214px) 100vw, 214px" /></span>
															</a>
															<p class="title"><a href="#" title="রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন">রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন</a></p>
												</li>


												<li>
															<a href="#" title="রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন" class="image-link">
																<span class="image">
																<img width="214" height="140" src="image/1.jpg" class="attachment-gallery-block size-gallery-block wp-post-image" alt="" title="" srcset="image/1.jpg" sizes="(max-width: 214px) 100vw, 214px" /></span>
															</a>
															<p class="title"><a href="#" title="রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন">রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন</a></p>
												</li>


												<li>
															<a href="#" title="রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন" class="image-link">
																<span class="image">
																<img width="214" height="140" src="image/1.jpg" class="attachment-gallery-block size-gallery-block wp-post-image" alt="" title="" srcset="image/1.jpg" sizes="(max-width: 214px) 100vw, 214px" /></span>
															</a>
															<p class="title"><a href="#" title="রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন">রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন</a></p>
												</li>


												<li>
															<a href="#" title="রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন" class="image-link">
																<span class="image">
																<img width="214" height="140" src="image/1.jpg" class="attachment-gallery-block size-gallery-block wp-post-image" alt="" title="" srcset="image/1.jpg" sizes="(max-width: 214px) 100vw, 214px" /></span>
															</a>
															<p class="title"><a href="#" title="রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন">রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন</a></p>
												</li>


												<li>
															<a href="#" title="রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন" class="image-link">
																<span class="image">
																<img width="214" height="140" src="image/1.jpg" class="attachment-gallery-block size-gallery-block wp-post-image" alt="" title="" srcset="image/1.jpg" sizes="(max-width: 214px) 100vw, 214px" /></span>
															</a>
															<p class="title"><a href="#" title="রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন">রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন</a></p>
												</li>


												<li>
															<a href="#" title="রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন" class="image-link">
																<span class="image">
																<img width="214" height="140" src="image/1.jpg" class="attachment-gallery-block size-gallery-block wp-post-image" alt="" title="" srcset="image/1.jpg" sizes="(max-width: 214px) 100vw, 214px" /></span>
															</a>
															<p class="title"><a href="#" title="রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন">রোটারী ক্লাবের উদ্যেগে ফ্রি চোখের ছানি অপারেশন ও ল্যান্স সংযোজন</a></p>
												</li>


										</ul>
										<div class="title-bar"></div>
								</div>
				</section>

			</div>
		</div>





		<aside class="col-4 sidebar">

			<ul>
				<li id="text-12" class="widget widget_text">
				<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fbraincoder.bd%2F&tabs=timeline&width=300&height=200&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=true&appId=105808716569469" width="300" height="200" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>

				</li>


					<li id="pw_login_widget-2" class=" widget widget_pw_login_widget">
						<div id="sp-main-div-pw_login_widget-2" class="sp-main-div sp-main-div-vertical" >
							<div class="sp-login-header"></div>
								<div class='sp-widget-login-div' >
										<form method="post" action="#" class="wp-user-form">
													<p>
														<label for='user_login-2'>Username: </label>
														<input id='user_login-2' type='text' name='log' required='required' /></p>
													<p>
														<label for='user_pass-2'>Password: </label>
														<input id='user_pass-2' type='password' name='pwd' required='required' />
													</p>
													<p><input type="submit" name="user-submit" value="Login" /></p>

										</form>

										<ul>
												<li><a class="sp-flipping-link" href='#sp-register'>Don't have an account?</a></li>
												<li><a class="sp-flipping-link" href='#lost-pass' >Lost your password?</a></li>
										</ul>
								</div>


								<div class='sp-widget-register-div' style='display: none'>
									<form method="post" action="#" class="wp-user-form">
												<p>
													<label for='reg_user_login-2'>Choose username: </label>
													<input id='reg_user_login-2' type='text' name='user_login' required='required' />
												</p>
												<p>
													<label for="user_email-2" >Your Email: </label>
													<input id="user_email-2" type="email" name="user_email" required="required" />
												</p>
												<p><input type="submit" name="user-submit" value="Sign up!" /></p>
									</form>

									<ul><li><a class="sp-flipping-link" href='#sp-login'>Have an account?</a></li></ul>
							</div>


							<div class='sp-widget-lost_pass-div' style='display:none;'>

										<form method="post" action='#'>
												<p><label for='lost_user_login-2'>Enter your username or email: </label>
												<input type="text" name="user_login" value="" size="20" id="lost_user_login-2" /></p>
												<p><input type="submit" name="user-submit" value="Reset my password"  /></p>
										</form>
										<ul><li><a class="sp-flipping-link" href='#sp-login'>Back to login</a></li></ul>
							</div>
						</div>
					</li>

					<li id="text-6" class="widget widget_text">
								<div class="textwidget">
									<div align="center">
										<a href="#" target="_blank"><img src="http://news.fenionline.net/wp-content/uploads/2015/06/feni_blog_advertise.jpg" alt="Feni Blog" class="no-display appear"></a>
									</div>
								</div>
					</li>

					<li id="bunyad_ads_widget-2" class="widget bunyad-ad">
						<div class="ads-widget">
							<a href="http://blog.fenionline.net" target="_blank"><img src="http://news.fenionline.net/wp-content/uploads/2015/06/feni_blog_advertise.jpg" alt="Feni Blog" class="no-display appear"></a>
						</div>
					</li>

					<li id="super_rss_reader-2" class="widget widget_super_rss_reader"><h3 class="widgettitle">সর্বশেষ ব্লগ পোস্ট</h3>
						<div class="super-rss-reader-widget">
							<div class="srr-wrap srr-vticker srr-style-grey" data-visible="5" data-speed="4000">
								<div>
										<div class="srr-item odd">
												<div class="srr-title">
													<a href="#" target="_blank" title="Posted on 15 December 2016">ছবির ফ্রেমে বাংলাদেশের ইতিহাস (১৯৫২ থেকে ১৯৭১)</a>
												</div>
												<div class="srr-meta"> - <cite class="srr-author">আতা স্বপন</cite></div>
												<p class="srr-summary srr-clearfix"> ইতিহাস আর ঐতিহ্যে এদেশের অমূল্য সম্পদ। আমাদের কৃষ্টি কালচার এমনি হাজারো ইতিহাস আর ঐহিহ্যের নিদর্শন
													<a href="#" title="Read more" target="_blank">[বিস্তারিত পড়ুন]</a>
												</p>
										</div>

										<div class="srr-item even">
												<div class="srr-title">
													<a href="#" target="_blank" title="Posted on 15 December 2016">ছবির ফ্রেমে বাংলাদেশের ইতিহাস (১৯৫২ থেকে ১৯৭১)</a>
												</div>
												<div class="srr-meta"> - <cite class="srr-author">আতা স্বপন</cite></div>
												<p class="srr-summary srr-clearfix"> ইতিহাস আর ঐতিহ্যে এদেশের অমূল্য সম্পদ। আমাদের কৃষ্টি কালচার এমনি হাজারো ইতিহাস আর ঐহিহ্যের নিদর্শন
													<a href="#" title="Read more" target="_blank">[বিস্তারিত পড়ুন]</a>
												</p>
										</div>


										<div class="srr-item odd">
												<div class="srr-title">
													<a href="#" target="_blank" title="Posted on 15 December 2016">ছবির ফ্রেমে বাংলাদেশের ইতিহাস (১৯৫২ থেকে ১৯৭১)</a>
												</div>
												<div class="srr-meta"> - <cite class="srr-author">আতা স্বপন</cite></div>
												<p class="srr-summary srr-clearfix"> ইতিহাস আর ঐতিহ্যে এদেশের অমূল্য সম্পদ। আমাদের কৃষ্টি কালচার এমনি হাজারো ইতিহাস আর ঐহিহ্যের নিদর্শন
													<a href="#" title="Read more" target="_blank">[বিস্তারিত পড়ুন]</a>
												</p>
										</div>

										<div class="srr-item even">
												<div class="srr-title">
													<a href="#" target="_blank" title="Posted on 15 December 2016">ছবির ফ্রেমে বাংলাদেশের ইতিহাস (১৯৫২ থেকে ১৯৭১)</a>
												</div>
												<div class="srr-meta"> - <cite class="srr-author">আতা স্বপন</cite></div>
												<p class="srr-summary srr-clearfix"> ইতিহাস আর ঐতিহ্যে এদেশের অমূল্য সম্পদ। আমাদের কৃষ্টি কালচার এমনি হাজারো ইতিহাস আর ঐহিহ্যের নিদর্শন
													<a href="#" title="Read more" target="_blank">[বিস্তারিত পড়ুন]</a>
												</p>
										</div>

								</div>
						 </div>
					 </div>
				 </li>




<li id="awesomeweatherwidget-2" class="widget widget_awesomeweatherwidget">
<div id="cont_c6a1cbc4eb59d0e4049af4d697be02a8"><script type="text/javascript" async src="https://www.theweather.com/wid_loader/c6a1cbc4eb59d0e4049af4d697be02a8"></script></div>
</li>





<li id="polls-widget-2" class="widget widget_polls-widget"><h3 class="widgettitle">জনমত জরিপ</h3><div id="polls-1" class="wp-polls">

			<form id="polls_form_1" class="wp-polls-form" action="#" method="post">

				<p style="text-align: center; font-size:16px;"><strong>সাইট কেমন লাগছে?</strong></p>
				<div id="polls-1-ans" class="wp-polls-ans">
						<ul class="wp-polls-ul">
									<li><input type="radio" id="poll-answer-1" name="poll_1" value="1" /> <label for="poll-answer-1">ভাল</label></li>
									<li><input type="radio" id="poll-answer-2" name="poll_1" value="2" /> <label for="poll-answer-2">খুবই ভাল</label></li>
									<li><input type="radio" id="poll-answer-3" name="poll_1" value="3" /> <label for="poll-answer-3">খারাপ</label></li>
									<li><input type="radio" id="poll-answer-4" name="poll_1" value="4" /> <label for="poll-answer-4">আরো ভাল করা যায়</label></li>
									<li><input type="radio" id="poll-answer-5" name="poll_1" value="5" /> <label for="poll-answer-5">মন্তব্য নেই</label></li>
						</ul>

						<p style="text-align: center;">
							<input type="button" name="vote" value="Vote" class="Buttons" />
						</p>

						<p style="text-align: center;">
							<a href="#ViewPollResults"  title="View Results Of This Poll">View Results</a>
						</p>
				</div>
			</form>

		</div>

		<ul><li><a href="pollsarchive/index.html">পুর্ববর্তী জরিপ সমুহ</a></li></ul>

</li>


<li id="popularitypostswidget-2" class="widget popularitypostswidget"><h3 class="widgettitle">সপ্তাহের সর্বাধিক পঠিত</h3>
				<ul>
			 			<li>
								<span class="ppw-post-title">
									<a href="#" title="সোনাগাজীর গোপালগাঁওয়ে ৬ষ্ঠ শ্রেনীর ছাত্রীকে ধর্ষনের ভিডিও ধারন, মাদরাসা যাওয়া বন্ধ" rel="nofollow">সোনাগাজীর গোপালগাঁওয়ে ৬ষ্ঠ শ্রেনীর ছাত্রীকে ধর্ষনের ভিডিও ধা...</a>
								</span>
								<span class="post-stats">
									<br>
									<span class="ppw-views"></span><span class="ppw-comments"></span>
									<span class="ppw-date"></span>
								</span>
								<br>
								<a href="#" title="সোনাগাজীর গোপালগাঁওয়ে ৬ষ্ঠ শ্রেনীর ছাত্রীকে ধর্ষনের ভিডিও ধারন, মাদরাসা যাওয়া বন্ধ" >
						    <img width="214" height="140" src="image/1.jpg" class="attachment-gallery-block size-gallery-block wp-post-image" alt="" title="" srcset="image/1.jpg" sizes="(max-width: 214px) 100vw, 214px" />
							 </a>
					 </li>


					 <li>
							 <span class="ppw-post-title">
								 <a href="#" title="সোনাগাজীর গোপালগাঁওয়ে ৬ষ্ঠ শ্রেনীর ছাত্রীকে ধর্ষনের ভিডিও ধারন, মাদরাসা যাওয়া বন্ধ" rel="nofollow">সোনাগাজীর গোপালগাঁওয়ে ৬ষ্ঠ শ্রেনীর ছাত্রীকে ধর্ষনের ভিডিও ধা...</a>
							 </span>
							 <span class="post-stats">
								 <br>
								 <span class="ppw-views"></span><span class="ppw-comments"></span>
								 <span class="ppw-date"></span>
							 </span>
							 <br>
							 <a href="#" title="সোনাগাজীর গোপালগাঁওয়ে ৬ষ্ঠ শ্রেনীর ছাত্রীকে ধর্ষনের ভিডিও ধারন, মাদরাসা যাওয়া বন্ধ" >
							 <img width="214" height="140" src="image/1.jpg" class="attachment-gallery-block size-gallery-block wp-post-image" alt="" title="" srcset="image/1.jpg" sizes="(max-width: 214px) 100vw, 214px" />
							</a>
					</li>



					<li>
							<span class="ppw-post-title">
								<a href="#" title="সোনাগাজীর গোপালগাঁওয়ে ৬ষ্ঠ শ্রেনীর ছাত্রীকে ধর্ষনের ভিডিও ধারন, মাদরাসা যাওয়া বন্ধ" rel="nofollow">সোনাগাজীর গোপালগাঁওয়ে ৬ষ্ঠ শ্রেনীর ছাত্রীকে ধর্ষনের ভিডিও ধা...</a>
							</span>
							<span class="post-stats">
								<br>
								<span class="ppw-views"></span><span class="ppw-comments"></span>
								<span class="ppw-date"></span>
							</span>
							<br>
							<a href="#" title="সোনাগাজীর গোপালগাঁওয়ে ৬ষ্ঠ শ্রেনীর ছাত্রীকে ধর্ষনের ভিডিও ধারন, মাদরাসা যাওয়া বন্ধ" >
							<img width="214" height="140" src="image/1.jpg" class="attachment-gallery-block size-gallery-block wp-post-image" alt="" title="" srcset="image/1.jpg" sizes="(max-width: 214px) 100vw, 214px" />
						 </a>
				 </li>



			 </ul>
</li>

		<li id="bunyad_ads_widget-8" class="widget bunyad-ad">

			<div class="ads-widget">
					<a href="#" target="_blank"><img src="http://news.fenionline.net/wp-content/uploads/2015/06/feni_blog_advertise.jpg" alt="Feni Blog" class="no-display appear"></a>
			</div>

		</li>

		<li id="t4bmpt" class="widget widget_muslim_prayer_time"><h3 class="widgettitle">Prayer Time Table</h3>
			<div class="muslim_prayer_time" align="center">

					<iframe id="iframe" style="width: 182px; height: 358px; border: 1px solid #ddd;" frameborder="0" scrolling="no" src="https://www.islamicfinder.org/prayer-widget/1185241/shafi/1/0/18.0/18.0"> </iframe>
			</div>
		</li>


		<li id="bunyad_ads_widget-8" class="widget bunyad-ad">

			<div class="ads-widget">
					<a href="#" target="_blank"><img src="http://news.fenionline.net/wp-content/uploads/2015/06/feni_blog_advertise.jpg" alt="Feni Blog" class="no-display appear"></a>
			</div>

		</li>

		<li id="bunyad_ads_widget-8" class="widget bunyad-ad">

			<div class="ads-widget">
				<h3 class="widgettitle">This News Titel</h3>
					<iframe width="300" height="200" src="https://www.youtube.com/embed/RHLPotBF7CQ" frameborder="0" allowfullscreen></iframe>
			</div>

		</li>



			</ul>
		</aside>
	</div> <!-- .row -->
</div> <!-- .main -->


<div style=" text-align:center; color:black; font-size:15px;">জনপ্রিয় বিষয় সমূহ</div><br>
<div style="text-align:center">
		<a style="margin-top:5px" href="#" class="button">পড়াশোনা</a>
		<a style="margin-top:5px" href="#" class="button">পড়াশোনা</a>
		<a style="margin-top:5px" href="#" class="button">পড়াশোনা</a>
		<a style="margin-top:5px" href="#" class="button">পড়াশোনা</a>
		<a style="margin-top:5px" href="#" class="button">পড়াশোনা</a>
		<a style="margin-top:5px" href="#" class="button">পড়াশোনা</a>
		<a style="margin-top:5px" href="#" class="button">পড়াশোনা</a>
		<a style="margin-top:5px" href="#" class="button">পড়াশোনা</a>
		<a style="margin-top:5px" href="#" class="button">পড়াশোনা</a>
		<a style="margin-top:5px" href="#" class="button">পড়াশোনা</a>
		<a style="margin-top:5px" href="#" class="button">পড়াশোনা</a>
		<a style="margin-top:5px" href="#" class="button">পড়াশোনা</a>
		<a style="margin-top:5px" href="#" class="button">পড়াশোনা</a>
 </div><br>




	<footer class="main-footer">
		<div class="wrap">
				<ul class="widgets row cf">

				<li class="widget col-4 widget_utcw widget_tag_cloud"><h3 class="widgettitle">News</h3>

						<a href="#">Home</a><br>
						<a href="#">Bangladeshi News</a><br>
						<a href="#">International News</a><br>
						<a href="#">CNN News</a><br>
						<a href="#">Finance News</a><br>
						<a href="#">Sports News</a><br>
						<a href="#">More News</a><br>


				</li>

					<li class="widget col-4 widget_text"><h3 class="widgettitle">News Source</h3>			<div class="textwidget">CNN NEWS<br />
						<!-- বাংলানিউজ২৪<br />
						দৈনিক সমকাল<br />
						দৈনিক কালের কণ্ঠ<br />
						দৈনিক আমাদের সময়<br />
						দৈনিক নয়া দিগন্ত<br />
						দৈনিক ইনকিলাব<br />
						দৈনিক ইত্তেফাক<br />
						বাংলাদেশ প্রতিদিন<br />
						আলোকিত বাংলা<br />
						ফেনীর সময়<br />
						নোয়াখালী নিউজ</div> -->
					</li>


					<li class="widget col-4 ">
						<h3 class="widgettitle">Desgin & Develop By </h3>
							<img style="height: 115px;" src="image/braincoder_logo.jpg"><br>Braincoder<br>
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fbraincoder.bd%2F&width=98&layout=button&action=like&size=small&show_faces=true&share=true&height=65&appId=574094369448691" width="98" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							<br>Contact Us:&nbsp; &nbsp;+8801751-783698<br>
							Website: braincoder.66ghz.com
							<a></a>
					</li>
				</ul>

		</div>





			<div class="lower-foot">
				<div class="wrap">
						<div class="widgets">
									<div class="textwidget">
											<div align="center" style="font-size:12px;">
												প্রকাশকঃ মোঃ শরিফুল ইসলাম ,সম্পাদকঃ ইলিয়াস আহমেদ খান , সহ- সম্পাদক আফিয়া ইসলাম, বার্তা সম্পাদকঃ আবু সাহমিন, বার্তা বিভাগ ও বানিজ্যিক কার্যালয়ঃ বাসা নংঃ০১ (৬ষ্ঠ তালা),রোড নংঃ০৫,সেক্টর নংঃ০১,উত্তরা মডেল টাউন ঢাকা। ফোনঃ +৮৮-০৪৪-৭৮৪২৮৪৩২,নিউজ রুম সেলঃ+৮৮-০১৬-১৪২৫২৬২৪ ইমেলঃeditor.newsbanglalive@gmail.com,newsbanglalive.desk@gmail.com
												<br>
												<span>Site Desgin & Develop By :<a href="https://www.facebook.com/braincoder.bd/">Braincoder</a></span>
											</div>
									</div>
						</div>
				</div>
		</div>


	</footer>

</div> <!-- .main-wrap -->


 <!-- Photo View <script>window._popup_data = {"ajaxurl":"http:\/\/news.fenionline.net\/wp-admin\/admin-ajax.php","do":"get_data","ajax_data":{"orig_request_uri":"\/"}};</script> -->

<link rel='stylesheet' id='popularity-posts-widget-css'  href="{{URL::asset('website/wp-content/plugins/popularity-posts-widget/style/ppw1c9b.css?ver=4.6.1')}}" type='text/css' media='all' />
<script type='text/javascript' src="{{URL::asset('website/wp-includes/js/comment-reply.min1c9b.js?ver=4.6.1')}}"></script>
<script type='text/javascript' src="{{URL::asset('website/wp-content/plugins/contact-form-7/includes/js/jquery.form.mind03d.js?ver=3.51.0-2014.06.20')}}"></script>

<script type='text/javascript' src="{{URL::asset('website/wp-content/plugins/contact-form-7/includes/js/scripts11b8.js?ver=4.5')}}"></script>

<script type='text/javascript' src="{{URL::asset('website/wp-content/plugins/jquery-t-countdown-widget/js/jquery.t-countdown7fb9.js?ver=1.5.9')}}"></script>

<script type='text/javascript' src='{{URL::asset('website/wp-content/plugins/wp-polls/polls-jse0ef.js?ver=2.73.1')}}'></script>
<script type='text/javascript' src='{{URL::asset('website/wp-content/themes/FeniNews/js/bunyad-theme1c9b.js?ver=4.6.1')}}'></script>
<script type='text/javascript' src="{{URL::asset('website/wp-content/themes/FeniNews/js/jquery.flexslider-min1c9b.js?ver=4.6.1')}}"></script>

<script type='text/javascript' src="{{URL::asset('website/wp-content/plugins/nice-login-register-widget/js/pw-login-widget264f.js?ver=1.3.10')}}"></script>
<script type='text/javascript' src="{{URL::asset('website/wp-content/plugins/nice-login-register-widget/js/ajax-authentication264f.js?ver=1.3.10')}}"></script>
<script type='text/javascript' src="{{URL::asset('website/wp-content/plugins/custom-sidebars/inc/external/wpmu-lib/js/wpmu-ui.3.min1c9b.js?ver=4.6.1')}}"></script>
<script type='text/javascript' src="{{URL::asset('website/wp-content/plugins/wordpress-popup/js/public.min1c9b.js?ver=4.6.1')}}"></script>
<script type='text/javascript' src="{{URL::asset('website/wp-includes/js/wp-embed.min1c9b.js?ver=4.6.1')}}"></script>

</body>

<!-- Mirrored from news.fenionline.net/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 31 Dec 2016 17:22:18 GMT -->
</html>
