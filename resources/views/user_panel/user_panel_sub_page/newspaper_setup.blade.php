@extends('user_panel.index')
@section('titel','Newspaper Setup')
@section('var_cumb','Newspaper Setup')
@section('link',"/Newspaper-setup")
@section('Content')


  {!! Form::open(['url' => 'foo/bar']) !!}


  <div style="margin-top: 50px;">
    <div class="container">
      <div class="col-md-12">
            <div class="col-sm-1"></div>
            <div class="col-sm-9">
              <div class="panel panel-default">
                <div class="panel-heading">Company Basic Information</div>
                <div class="panel-body">

                  <div class="col-sm-12">
                      <div class="col-sm-3">Company Name</div>
                      <div class="col-sm-9"><input type="text" class="form-control"></div>
                  </div>
                  <hr>



                  <div class="col-sm-12">
                      <div class="col-sm-3">Company abbr</div>
                      <div class="col-sm-9"><input type="text" class="form-control"></div><hr>
                  </div>


                  <div class="col-sm-12">
                      <div class="col-sm-3">Established</div>
                      <div class="col-sm-9"><input type="Date" class="form-control"></div><hr>
                  </div>

                  <div class="col-sm-12">
                      <div class="col-sm-3">Email</div>
                      <div class="col-sm-9"><input type="Email" class="form-control"></div><hr>
                  </div>


                  <div class="col-sm-12">
                      <div class="col-sm-3">Mobile</div>
                      <div class="col-sm-9"><input type="text" class="form-control"></div><hr>
                  </div>


                  <div class="col-sm-12">
                      <div class="col-sm-3">Telephone</div>
                      <div class="col-sm-9"><input type="text" class="form-control"></div><hr>
                  </div>



                  <div class="col-sm-12">
                      <div class="col-sm-3">Registration Number</div>
                      <div class="col-sm-9"><input type="text" class="form-control"></div><hr>
                  </div>

                  <div class="col-sm-12">
                      <div class="col-sm-3">Facebook Api</div>
                      <div class="col-sm-9"><input type="text" class="form-control"></div><hr>
                  </div>


                  <div class="col-sm-12">
                      <div class="col-sm-3">Twitter</div>
                      <div class="col-sm-9"><input type="text" class="form-control"></div><hr>
                  </div>

                  <div class="col-sm-12">
                      <div class="col-sm-3">Office Location(Google Api)</div>
                      <div class="col-sm-9"><input type="text" class="form-control"></div><hr>
                  </div>

                


                  <div class="col-sm-12" style="margin-top: 22px;">
                      <link href="css/bootstrap-imgupload.css" rel="stylesheet">
                      <form>
                          <div class="form-group">
                            <div class="imgupload panel panel-default">
                              <div class="panel-heading clearfix">
                                <h3 class="panel-title pull-left">Upload Logo</h3>

                              </div>

                              <div class="file-tab panel-body">
                                <div>

                                  <input class='btn btn-default' type="file" name="file-input">


                                </div>
                              </div>

                            </div>
                          </div>
                        </form>
                      </div>


                  <div class="text-center col-sm-12">
                      <input type="submit" name="" class="btn btn-primary">
                  </div>




                    </div>
              </div>
            </div>
            <div class="col-sm-2"></div>

      </div>
    </div>
  </div>
{!! Form::close() !!}
@endsection
