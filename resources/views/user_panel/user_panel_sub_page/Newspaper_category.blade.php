@extends('user_panel.index')
@section('titel','Newspaper Category')
@section('var_cumb','Newspaper Category')
@section('link',"/category")
@section('Content')

<div class="container" style="margin-top: 30px;">
  <div class="row">
      <div class="col-sm-12">
          <a class="btn btn-default" href="#" data-modal-id="popup1"><i class="fa fa-plus" aria-hidden="true"></i></a>

      </div>
  </div>
</div>




<div id="popup1" class="modal-box">
  <header> <a href="#" class="js-modal-close close">×</a>
    <h3>Create Category</h3>
  </header>


  <div class="modal-body">

      <div class="col-sm-12">
            <div class="col-sm-4">Titel</div>
            <div class="col-sm-8"><input type="text" class="form-control"></div>
      </div>

      <br><br>

      <div class="col-sm-12">
            <div class="col-sm-4">Caret</div>
            <div class="col-sm-8"><input type="text" class="form-control"></div>
      </div>

      <br><br>


      <div class="col-sm-12">
            <div class="col-sm-4">Slug</div>
            <div class="col-sm-8"><input type="text" class="form-control"></div>
      </div>

    
      <br><br>

      <div class="col-sm-12">
            <div class="col-sm-4">Status</div>
            <div class="col-sm-8">
              <select class="form-control">
                  <option>Open</option>
                  <option>Publish</option>
              </select>
            </div>
      </div>

      <br><br>


      <div class="col-sm-12">
            Description
      </div>

      <div class="col-sm-12">
            <textarea class="form-control textarea"></textarea>
      </div>

      <br><br><br><br>
  </div>

  <footer> <a href="#" class="btn btn-small js-modal-close">Close</a> </footer>

</div>









<div id="See_report" class="col-sm-12" style="font-size:15px">
            <div class="col-sm-12" style="font-family:Times new roman">
        <div class="panel panel-default filterable">
            <div class="panel-heading">
                <h3 class="panel-title">Category Report </h3>
                <div class="pull-right">
                    <button type="button" class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                </div>
            </div>
            <div class="panel-body">
              <table class="table table-striped table-responsive table-bordered table-list">
                  <thead>
                    <tr class="filters">
                        <th style="width: 6%;"><input type="text" class="form-control" placeholder="Action" disabled=""></th>
                        <th><input type="text" class="form-control" placeholder="Status" disabled=""></th>
                        <th><input type="text" class="form-control" placeholder="Titel" disabled=""></th>
                        <th><input type="text" class="form-control" placeholder="caret" disabled=""></th>
                        <th><input type="text" class="form-control" placeholder="Parent Category" disabled=""></th>


                    </tr>
                  </thead>
                  <tbody>

                            <tr style="font-size: 12px;">
                              <td style="width: 11%;">
        <a class="text-danger" href="#"><i class="fa fa-comment" aria-hidden="true"></i></a>
        <a href="#" onclick="return confirm('Are you absolutely sure you want to delete?')"><i class="fa fa-trash text-danger" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-times text-danger" aria-hidden="true"></i></a>
        <a target="_blank" href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                </td>

                                <td style="width: 11%;"><span class="text-success"><i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;Publish</span></td>
                                <td>ss</td>
                                <td>14th Batch</td>
<td><span class="badge">1</span></td>
                            </tr>



                        </tbody>
                </table>
              </div>


        </div>
    </div>



            </div>



<script src="{{URL::asset('js/table_data.js')}}"></script>
<link href="{{URL::asset('css/table_data.css')}}" rel='stylesheet' type='text/css'>
@stop
