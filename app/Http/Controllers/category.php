<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class category extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      return view('user_panel.user_panel_sub_page.Newspaper_category');
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      //
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //
  }

  // use CommonController;
  // /**
  //  * RoleController constructor.
  //  */
  // public function __construct()
  // {
  //     $this->view_dir = 'user_panel.category';
  //     $this->model = new Category();
  // }
  // /**
  //  * @param null $data
  //  * @return array
  //  */
  // protected function validate_rules($data = null) {
  //     return [
  //         'name' => 'Required',
  //         'slug' => 'Required|Unique:roles'.($data && $data->id?',slug,'.$data->id:'')
  //     ];
  // }
}
