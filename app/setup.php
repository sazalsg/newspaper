<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class setup extends Model
{
  protected $primaryKey='id';
    protected $table='users';
    protected $hidden = array('password', 'remember_token');
    public $timestamps = false;
    protected $fillable = [
        'title',
        'snippet'
    ];
}
